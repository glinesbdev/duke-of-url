module DukeOfUrl
  module Aliases

    ::DukeOfUrl::Preferences.register({ })

    def self.find(raw_input)
      verb = ::DukeOfUrl::Input::Parser.new(raw_input).first_word
      ali = read.find{|alius| verb == alius.verb.strip}
      return raw_input unless ali
      return raw_input if ! ali.args? && verb != raw_input
      ali.raw_input = raw_input
      ali
    end

    def self.read
      Alias.order_by(:verb).all
    end

    def self.save!(input)
      Alias.save!(input)
    end

    def self.typeahead
      read.map(&:verb)
    end

    module AliasDB
      def self.setup!

        DUKE_DB.create_table?(:aliases) do
          primary_key :id
          String :verb, null: false
          String :template, null: false
          DateTime :recent, null: false
          DateTime :orig, null: false
        end

      end
    end
  end

  Aliases::AliasDB.setup!

  module Aliases

    module Routes
      def self.registered(app)
        app.get "/aliases/" do
          slim :aliases, layout: :layout
        end

        app.post "/delalias/:aid" do
          id = params['aid'].to_i
          bye = Alias[id]
          bye.destroy
          ''
        end
      end
    end

    class Alias < Sequel::Model(DUKE_DB[:aliases])

      def self.save!(raw_input)
        now = DateTime.now

        parsed = ::DukeOfUrl::Input::Parser.new(raw_input)
        return "refusing to create recursive alias" if parsed.recursive?

        verb = parsed.first_word
        verb = %Q{#{verb} } if parsed.args.include?('${*}')
        if og = self.find(verb: verb)
          og.update(template: parsed.args, verb: verb, orig: now, recent: now)
        else
          self.create(template: parsed.args, verb: verb, orig: now, recent: now)
        end
        %Q{#{verb} set to #{parsed.args}}
      end

      def args?
        template.include?('${*}')
      end

      def before_destroy
        pref = %q{default:destination}
        Preferences.unset(pref) if Preferences.fetch(pref) == verb.strip
      end

      def destination
        template.sub('${*}',magic_args)
      end

      def export
        {
          orig: orig,
          recent: recent,
          template: template,
          verb: verb,
        }
      end

      def export_as_input
        %Q{alias #{verb} #{template}}
      end

      def first_letter
        fl = verb[0]
        return '#' if fl.to_i.to_s == fl
        fl
      end

      def persist!
        DukeOfUrl::History.save!(raw_input) if history?
      end

      def raw_input=(raw_input)
        @raw_input = raw_input
      end

      def redirect?
        template_is_url?
      end

      def set?
        template.include?('set ')
      end

      def to_s
        %Q{#{verb} #{template}}
      end

      private

      # this should be a mixin as URL also has it
      def args
        raw_input.sub(/^\s*\S+\s+/,'')
      end

      # I guess we don't support positional params, just $*
      def escaped_args
        ERB::Util.url_encode(args)
      end

      def history?
        ! leading_space?
      end

      def leading_space?
        raw_input.match?(/^\s+/)
      end

      def magic_args
        return escaped_args if template_is_url?
        args
      end

      def raw_input
        @raw_input
      end

      def template_is_url?
        template.match?(VALID_URL_RE)
      end

    end
  end
end
