module DukeOfUrl
  def self.db_setup!
    endpoint = ENV.fetch('DUKE_DB', 'sqlite://userdata/duke.db')
    ::Sequel.connect(endpoint)
  end
  DUKE_DB = db_setup!
end
