module DukeOfUrl
  module Preferences
    Means_Yes  = /\b(yes|true|on|enabled)\b/i
    Means_No   = /\b(no|false|off|disabled)\b/i
    Valid_Bool = Regexp.union(Means_Yes, Means_No)

    DEFAULTS = {
      'default:destination' => {default: '', description: "The Alias to use when Duke wasn't able to figure out what to do with your input. Search engine suggsted.", valid: /.*/},
      'ui:background' => {default: '#222', description: "The CSS-valid background color. This will be applied to both background: and background-image: and can be any valid CSS value", valid: /.*/},
      'ui:button:color' => {default: '#0d6efd', description: "The CSS-valid color for buttons and probably some other stuff.", valid: /.*/},
      'ui:button:danger:color' => {default: 'red', description: "The CSS-valid color for SCARY buttons and probably some other stuff.", valid: /.*/},
      'ui:button:text:color' => {default: 'white', description: "The CSS-valid color for button text.", valid: /.*/},
      'ui:default:tab' => {default: 'history', description: 'Which tab to show by default. Valid: aliases bookmarks cloud history', valid: /^(aliases|bookmarks|cloud|history)$/ },
      'ui:font:primary' => {default: 'Oxanium, sans-serif', description: 'CSS-valid list of font-family fonts used by most of the UI', valid: /.*/ },
      'ui:font:secondary' => {default: 'Cinzel, serif', description: 'CSS-valid list of font-family fonts used by bits of the UI', valid: /.*/ },
      'ui:font:size' => {default: '16', description: 'Integer that will have "px" appended to it to set the font size.', valid: /\A\d+\Z/ },
      'ui:highlight' => {default: 'orange', description: 'The CSS-valid color used for accents here and there.', valid: /.*/ },
      'ui:hovershadow' => {default: 'var(--highlight-color) 0px 1px 0px, var(--highlight-color) 0px 0px 8px', description: 'The CSS-valid box-shadow definition used for hover indicators. Set to "0" to turn off these hover indicators.', valid: /.*/ },
      'ui:link:color' => {default: '#0d6efd', description: 'The CSS-valid color used for link text.', valid: /.*/ },
      'ui:text:color' => {default: 'white', description: "The CSS-valid text color.", valid: /.*/},
    }
    def self.register(prefs)
      DEFAULTS.merge!(prefs)
    end

    module PreferencesDB
      def self.setup!

        DUKE_DB.create_table?(:prefs) do
          primary_key :id
          String :key, null: false
          String :value, null: false
        end

      end
    end

    PreferencesDB.setup!

    def self.bool(key)
      fetch(key).to_s.match?(Means_Yes)
    end

    def self.get(raw_input)
      Pref.where(key: raw_input.strip).first
    end

    def self.fetch(key)
      fromdb = get(key)
      return fromdb.value if fromdb
      DEFAULTS.fetch(key,{}).fetch(:default,'')
    end

    def self.int(key)
      fetch(key).to_i
    end

    def self.set(raw_input)
      Pref.save!(raw_input)
    end

    def self.unset(raw_input)
      if raw_input.include?('*')
        prefix    = raw_input.split('*').fetch(0,'WHOOPSIEBOOPS')
        raw_input = DEFAULTS.keys.select{|key| key.start_with?(prefix)}
      end

      Array(raw_input).each{|pref| Pref.remove!(pref) }
    end

    def self.typeahead
      DEFAULTS.keys.map{|pref| [%Q{set #{pref}}, %Q{unset #{pref}}] }.flatten
    end

    class Pref < Sequel::Model(DUKE_DB[:prefs])

      def self.save!(raw_input)
        parsed = ::DukeOfUrl::Input::Parser.new(raw_input)
        return unless DEFAULTS[parsed.first_word]
        return "Ignoring invalid value for pref #{parsed.first_word}" unless parsed.args.match?(DEFAULTS[parsed.first_word][:valid])

        og = self.where(key: parsed.first_word).first
        if og
          if parsed.args == DEFAULTS[parsed.first_word][:default].to_s
            return og.destroy
          end
          return og.update(value: parsed.args)
        end

        self.create(key: parsed.first_word, value: parsed.args)
      end

      def self.remove!(raw_input)
        og = self.where(key: raw_input).first
        og.destroy if og
      end

      def self.uiprefs
        where(Sequel.like(:key, 'ui:%'))
      end

      def to_s
        %Q{#{key} set to #{value}}
      end

    end

    module Routes
      def self.registered(app)
        app.get "/preferences/" do
          @defaults   = DEFAULTS
          slim :preferences, layout: :layout
        end
      end
    end
  end

  module Input
    module Handlers

      class PrefViewer < AbstractHandler

        def self.for(input)
          input.strip == verb
        end

        def self.verb
          %q{preferences}
        end

        def destination
          %q{/preferences/}
        end

        def redirect?
          true
        end
      end

      class PrefSet < AbstractHandler

        def self.verb
          %q{set }
        end

        def message
          @message
        end

        def persist!
          @message = Preferences.set(args)
        end

        def redirect?
          false
        end
      end

      class PrefUnset < AbstractHandler

        def self.verb
          %q{unset }
        end

        def persist!
          Preferences.unset(args)
        end

        def redirect?
          false
        end
      end

      class SaveUi < AbstractHandler
        def self.verb
          %q{saveui }
        end

        def form_value
          @message = %Q{alias #{last_word} #{not_defaults.map{|pref| "set #{pref.key} #{pref.value}"}.join(' && ')}}
        end

        def redirect?
          false
        end

        private

        def not_defaults
          ::DukeOfUrl::Preferences::Pref.uiprefs.all
        end
      end

    end
  end
end
