module DukeOfUrl
  module Help
    module Routes
      def self.registered(app)
        app.get '/help/' do
          @references = JSON.parse(File.read('lib/help_reference.json')).tap do |references|
            references.each_key do |key|
              references[key].sort_by { |section| section['name'] }
            end
          end.sort

          slim :help, layout: :layout
        end
      end
    end
  end
end
