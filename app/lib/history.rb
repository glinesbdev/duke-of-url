module DukeOfUrl

  module History

    ::DukeOfUrl::Preferences.register( {
      'history:show' => {default: 20, description: 'The number of history entries to show in the UI.', valid: /^\d+/ },
      'history:verbose' => { default: 'false', description: 'Show visit statistics for history entries.', valid: ::DukeOfUrl::Preferences::Valid_Bool },
    })

    def self.read(limit=10)
      HistoryEntry.most_recent(limit)
    end

    def self.save!(input)
      HistoryEntry.save!(input)
    end

    def self.typeahead
      HistoryEntry.default_sort.all
    end

    module HistoryDB
      def self.setup!

        DUKE_DB.create_table?(:history) do
          primary_key :id
          String :text, null: false
          DateTime :recent, null: false
          DateTime :orig, null: false
          Integer :visit_count, default: 0
        end

      end
    end
  end

  History::HistoryDB.setup!

  module History

    module Routes
      def self.registered(app)
        app.get "/history/" do
          if params["q"] && ! params["q"].strip.empty?
            @history = HistoryEntry.default_sort.where(Sequel.like(:text, %Q{%#{params['q']}%})).all
          else
            @history = HistoryEntry.default_sort.all
          end
          slim :history, layout: :layout
        end

        app.post '/deletehistory/:hid' do
          id = params['hid'].to_i
          bye = History::HistoryEntry.where(id: id).first
          bye.destroy
          ''
        end

      end
    end

    class HistoryEntry < Sequel::Model(DUKE_DB[:history])
      def self.save!(raw_input)
        now = DateTime.now
        og = self.find(text: raw_input)
        return og.update(recent: now, visit_count: og.visit_count + 1) if og
        self.create(text: raw_input, recent: now, orig: now, visit_count: 1)
      end

      def self.bangbang
        default_sort.first
      end

      def self.default_sort
        self.order(Sequel.desc(:recent))
      end

      def self.most_recent(limit=1)
        default_sort.limit(limit)
      end

      def ago
        ::DukeOfUrl::Formatters::Seconds.format( Time.now - recent )
      end

      def args
        words[1..-1].join(' ')
      end

      def encoded
        ERB::Util.url_encode(text)
      end

      def export
        {
          orig: orig,
          recent: recent,
          text: text,
          visit_count: visit_count,
        }
      end

      def export_as_input
        text
      end

      def last_word
        words.last
      end

      def to_json(*a)
        text.to_json(*a)
      end

      def to_s
        text
      end

      def words
        text.split(/\s+/)
      end
    end
  end

end
