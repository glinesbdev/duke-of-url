module DukeOfUrl
  module Input

    class AbstractHandler
      include ::DukeOfUrl::Parsers::UserInput

      def self.for(input)
        input.start_with?(verb)
      end

      def initialize(raw_input)
        @raw_input = raw_input
      end

      def destination
      end

      def message
      end

      def persist!
        DukeOfUrl::History.save!(input) if history?
      end

      def redirect?
        true
      end

      def verb
        return self.class.verb if self.class.respond_to?(:verb)
        ''
      end

      private

      def history?
        false
      end
    end

    class MultiCmd < AbstractHandler
      def self.for(input)
        input.include?(" && ") && ! input.start_with?("alias ")
      end

      def commands
        @commands ||= raw_input.split(' && ').map{|cmd| ::DukeOfUrl::Input.process(cmd)}
      end

      def destination
        finally.destination
      end

      def message
        finally.message
      end

      def persist!
        commands.each(&:persist!)
      end

      def redirect?
        finally.redirect?
      end

      private

      def finally
        @finally ||= commands.last
      end
    end


    module Handlers

      class URL < AbstractHandler

        def self.for(input)
          input.match?(VALID_URL_RE)
        end

        def destination
          raw_input
        end

        private

        def history?
          ! leading_space?
        end
      end

      class Math < AbstractHandler

        def self.for(input)
          input.match?( /^[\s\d\(\)\+%\/*.-]+$/ )
        end

        def message
          begin
            eval(input)
          rescue
            "don't get cute"
          end
        end

        def redirect?
          false
        end
      end

      class TLD < AbstractHandler
        # https://jecas.cz/tld-list/?type=plain
        LOL = JSON.parse(File.read('lib/tlds.json'))
        def self.for(input)
          input = input.upcase
          ! input.strip.match(/\s/) && LOL.find{|tld| input.end_with?(".#{tld}")}
        end

        def destination
          %Q{https://#{input}}
        end

        private

        def history?
          ! leading_space?
        end
      end

      class Alias < AbstractHandler

        def self.verb
          %q{alias }
        end

        def message
          @message
        end

        def persist!
          @message = Aliases.save!(args)
        end

        def redirect?
          false
        end
      end

      class AliasReturn < Alias
        def self.verb
          %q{aliasr }
        end

        def destination
          last_word
        end

        def redirect?
          true
        end

      end

      class AliasBrowser < AbstractHandler

        def self.verb
          %q{aliases}
        end

        def destination
          '/aliases/'
        end
      end

      class AliasEditor < AbstractHandler

        def self.verb
          %q{edit alias }
        end

        def form_value
          %Q{alias #{Aliases.find(args)}}
        end

        def redirect?
          false
        end
      end

      class UnAlias < AbstractHandler

        def self.verb
          %q{unalias }
        end

        def persist!
          maybestr = Aliases.find(words[1])
          maybestr.destroy if maybestr.respond_to?(:destroy)
        end

        def redirect?
          false
        end

      end

      class History < AbstractHandler

        def self.for(input)
          input.strip == verb
        end

        def self.verb
          %q{history}
        end

        def destination
          %q{/history/}
        end
      end

      class HistorySearch < AbstractHandler

        def self.verb
          %q{history? }
        end

        def destination
          %Q{/history/?q=#{escaped_args}}
        end

      end

      class Help < AbstractHandler

        def self.verb
          %q{help}
        end

        def destination
          %q{/help/}
        end
      end

    end
  end
end
