module DukeOfUrl
  module Plugins
    def self.route_modules
      constants.map do |plugin_const|
        plugin = DukeOfUrl::Plugins.const_get(plugin_const)
        plugin.const_get(:Routes) if plugin.constants.include?(:Routes)
      end.compact
    end
    Dir.glob('plugins/*.rb').each{|path| module_eval( File.read(path) )}
  end
end
