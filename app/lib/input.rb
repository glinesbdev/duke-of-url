require_relative 'handlers'

module DukeOfUrl
  VALID_URL_RE = Regexp.new('^(https{0,1}|file)://')

  module Substitutions
    def self.fetch(raw_input)
      return raw_input unless raw_input.match?(/!/)
      return History::HistoryEntry.bangbang.text if raw_input == '!!'
      return raw_input.sub('!!',History::HistoryEntry.bangbang.text) if raw_input.include?('!!')

      if /^!(\d+)/.match(raw_input)
        hist_num = $~.captures.first.to_i
        return History::HistoryEntry.most_recent(hist_num).all.last.text
      end

      if /^![?](.*)/.match(raw_input)
        needle = $~.captures.first.strip
        found = History::HistoryEntry.where(Sequel.like(:text, %Q{%#{needle}%})).last
        return '' unless found
        return found.text
      end

      # todo !-x$ etc
      if /![$]/.match?(raw_input)
        last_word = History::HistoryEntry.bangbang.last_word
        return raw_input.sub(/![$]/,last_word)
      end

      if /![*]/.match?(raw_input)
        args = History::HistoryEntry.bangbang.args
        return raw_input.sub(/![*]/,args)
      end

      return raw_input
    end
  end

  module Input

    def self.commands
      handlers.select{|handler| handler.respond_to?(:verb)}.map(&:verb)
    end

    def self.handlers
      Handlers.constants.map{|const_sym| DukeOfUrl::Input::Handlers.const_get(const_sym)}
    end

    def self.process(raw_input)
      return MultiCmd.new(raw_input) if MultiCmd.for(raw_input)

      raw_input = Substitutions.fetch(raw_input)
      return if raw_input.empty?

      alius = Aliases.find(raw_input)
      if alius != raw_input
        return alius if alius.redirect?
        return MultiCmd.new(alius.destination) if MultiCmd.for(alius.destination)
        # the alias wasn't to a website but instead to a Duke command
        raw_input = alius.destination
      end

      klass = handlers.find{|plugin| plugin.for(raw_input)} || UhOh
      print "I think #{raw_input} is a: "
      pp klass
      klass.new(raw_input)
    end


    class Parser
      include ::DukeOfUrl::Parsers::UserInput
      def initialize(raw_input)
        @raw_input = raw_input
      end
      def verb
        ''
      end
    end

    class UhOh
      def initialize(raw_input)
        @raw_input = raw_input
      end

      def destination
        return alius.destination if pref?
      end

      def message
        return %Q{#{@raw_input} command not found} unless pref?
        ''
      end

      def persist!
        alius.persist! if pref?
      end

      def redirect?
        return alius.redirect? if pref?
        false
      end

      private

      def alius
        @alius ||= get_alius
      end

      def get_alius
        alius = Aliases::Alias.where(verb: verb).first
        alius.raw_input = %Q{#{alius.verb} #{raw_input}}
        alius
      end

      def pref
        @pref ||= Preferences.fetch('default:destination')
      end

      def pref?
        ! pref.empty?
      end

      def raw_input
        @raw_input
      end

      def verb
        %Q{#{pref} }
      end
    end

  end
end
