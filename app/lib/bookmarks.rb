module DukeOfUrl
  module Bookmarks

    ::DukeOfUrl::Preferences.register( {
      'bookmarks:show' => {default: 20, description: 'The number of bookmarks to show in the UI.', valid: /^\d+/ },
    } )

    module BookmarkDB
      def self.setup!

        DUKE_DB.create_table?(:bmtags) do
          primary_key :tid
          String :tag, null: false
          Integer :bmid, null: false
        end

        DUKE_DB.create_table?(:bm) do
          primary_key :id
          String :url, null: false
          String :notes
        end

        DUKE_DB.add_index(:bm, :url, ignore_errors: true)

      end

    end

    Bookmarks::BookmarkDB.setup!

    def self.retag!(raw_input)
      oldtag, *newtags = raw_input
      Tag.where(tag: oldtag).each do |existing|
        # replace the old tag
        newtag = newtags.shift
        existing.update(tag: newtag)
        # if there are more, make them
        bmid = existing.bmid
        newtags.each do |newtag|
          newtag = newtag.delete(',')
          next if Tag.where(bmid: bmid, tag: newtag).first
          Tag.create(tag: newtag, bmid: bmid)
        end
      end
    end

    def self.save!(raw_input)
      Bookmark.save!(raw_input)
    end

    def self.bookmarked?(url)
      Bookmark.where(url: url)
    end

    class Bookmark < Sequel::Model(DUKE_DB[:bm])
      def self.save!(raw_input)
        url, *tags = *::DukeOfUrl::Input::Parser.new(raw_input).shell_words

        tags = tags.join(' ').split(',').map(&:strip)

        bm = self.where(url: url).first
        if bm
          Tag.where(bmid: bm.id).all.each{|tag| tag.destroy}
        else
          bm = self.create(url: url, notes: '')
        end
        bmid = bm.id
        tags.each do |tag|
          Tag.create(tag: tag, bmid: bmid)
        end
      end

      def self.with_tags(tags)
        tagcount = Array(tags).length
        self.from(:bm, :bmtags).group_and_count(:bm__id).where(bmtags__bmid: :bm__id).where(bmtags__tag: tags).having{count.function.* >= tagcount}.select_append(:bm__url).all
      end

      def export
        {
          tags: tags.map(&:tag),
          url: url,
        }
      end

      def export_as_input
        %Q{bm #{url} #{tags.map(&:tag).join(", ")}}
      end

      def tags
        Tag.where(bmid: id).sort_by(&:tag)
      end

      def encoded_url
        ERB::Util.url_encode(url)
      end

    end

    class Tag < Sequel::Model(DUKE_DB[:bmtags])
      def self.cloud
        # "SELECT `tag`, count(*) AS 'c' FROM `bmtags` GROUP BY `tag`"
        self.group_and_count(:tag)
      end

      def bookmark
        Bookmark.where(id: bmid).first
      end

      def bookmark_count
        Tag.where(tag: tag).count
      end

      def cloud_factor(max_value)
        max_font_size    = 80.0
        min_font_size    = 8.0
        range            = max_font_size - min_font_size
        countf           = count.to_f
        factor           = (countf / max_value)
        scaled_font_size = (range * factor) + min_font_size - (min_font_size * factor)
        %Q{#{scaled_font_size}pt}

      end

      def count
        @values.fetch(:count,0)
      end

      def encoded_tag
        ERB::Util.url_encode(tag)
      end

      def to_json(*a)
        %Q{"#{tag}"}
      end
    end


    module Routes
      def self.registered(app)

        app.get '/bookmarks/' do
          @params    = params
          if params['bms']
            parsed = ::DukeOfUrl::Input::Parser.new(params['bms'])
            wild = %Q{%#{parsed.args}%}
            @bookmarks = Bookmark.order_by(Sequel.desc(:id)).where(Sequel.like(:url, wild)).all
          else
            @bookmarks = Bookmark.order_by(Sequel.desc(:id)).all
          end
          slim :bookmarks, layout: :layout
        end

        app.post '/delbookmark/:bmid' do
          id = params['bmid'].to_i
          the = Bookmark.where(id: id).first
          the.destroy
          Tag.where(bmid: id).each{|tag| tag.destroy}
          ''
        end

        app.get '/bookmarks/:tags' do
          tags = params["tags"].split(',').map(&:strip)
          @bookmarks = Bookmark.with_tags(tags)
          slim :bookmarks, layout: :layout
        end

      end
    end
  end

  module Input
    module Handlers

      class Bookmark < AbstractHandler

        def self.verb
          %q{bm }
        end

        def persist!
          Bookmarks.save!(args)
        end

        def redirect?
          false
        end
      end

      class BookmarkReturn < Bookmark

        def self.verb
          %q{bmr }
        end

        def destination
          words[1]
        end

        def redirect?
          true
        end
      end

      class BookmarkBrowser < AbstractHandler

        def self.for(input)
          input.strip == verb
        end

        def self.verb
          %q{bms}
        end

        def destination
          %Q{/bookmarks/}
        end

      end

      class BookmarkRetag < AbstractHandler

        def self.verb
          %q{bmretag }
        end

        def message
          %Q{"#{shell_words_args.first}" retagged to #{shell_words_args[1..-1].join(' ')}}
        end

        def persist!
          Bookmarks.retag!(shell_words_args)
        end

        def redirect?
          false
        end

      end

      class BookmarkSearch < AbstractHandler

        def self.verb
          %q{bm? }
        end

        def destination
          %Q{/bookmarks/?bms=#{escaped_args}}
        end
      end

      class BookmarkTag < AbstractHandler

        def self.verb
          %q{bmt }
        end

        def initialize(raw_input)
          @raw_input = raw_input
        end

        def destination
          %Q{/bookmarks/#{escaped_args}}
        end
      end

    end
  end
end
