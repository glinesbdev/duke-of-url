require 'rubygems'
require 'bundler'
Bundler.setup
require 'pp'
require 'date'
require 'shellwords'
require 'erb'
require 'json'
require 'yaml'

require 'sinatra'
require 'sinatra/json'
require 'tilt/sass'
require 'slim'
require 'sassc'

require 'sequel'

require_relative 'lib/db'
require_relative 'lib/junkdrawer'
require_relative 'lib/input'
require_relative 'lib/preferences'
require_relative 'lib/history'
require_relative 'lib/aliases'
require_relative 'lib/bookmarks'
require_relative 'lib/plugins'
require_relative 'lib/eximport'
require_relative 'lib/help'

Sequel.split_symbols = true

module DukeOfUrl

  class Web < ::Sinatra::Base
    set :bind, "0.0.0.0"
    set :public_folder, 'public'

    helpers do
      def prefs
        ::DukeOfUrl::Preferences
      end

      def sass(filefrag)
        file = %Q{views/sass/#{filefrag}.sass}
        Tilt::SassTemplate.new(file).render
      end

      def typeahead
        {
          commands: Input.commands,
          aliases:  Aliases.typeahead,
          preferences: Preferences.typeahead,
          history: History.typeahead
        }.to_json
      end
    end

    get '/css/*.css' do
      content_type 'text/css', :charset => 'utf-8'
      sass params[:splat].first
    end

    get '/' do
      histcount = prefs.int('history:show')
      @history =  DukeOfUrl::History.read(histcount)

      if params["q"] && ! params["q"].strip.empty?
        @handler = Input.process(params['q'])
        unless @handler.nil?
          @handler.persist!
          redirect @handler.destination if @handler.redirect?
        end
      end

      slim :index, layout: :layout
    end

    register Aliases::Routes
    register Bookmarks::Routes
    register History::Routes
    register Preferences::Routes
    register Export::Routes
    register Help::Routes

    Plugins.route_modules.each{|route_module| register route_module}

    get '/opensearch.xml' do
      @duke = "#{request.scheme}://#{request.host}:#{request.port}"
      headers "Content-Type" => "application/opensearchdescription+xml"
      slim :opensearch, layout: false
    end

  end
end
