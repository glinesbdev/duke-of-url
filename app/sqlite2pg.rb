require 'bundler'
Bundler.setup
require 'sequel'
require 'pp'

def setup!(conn)

  conn.create_table?(:aliases) do
    primary_key :id
    String :verb, null: false
    String :template, null: false
    DateTime :recent, null: false
    DateTime :orig, null: false
  end

  conn.create_table?(:bmtags) do
    primary_key :tid
    String :tag, null: false
    Integer :bmid, null: false
  end

  conn.create_table?(:bm) do
    primary_key :id
    String :url, null: false
    String :notes
  end

  conn.create_table?(:history) do
    primary_key :id
    String :text, null: false
    DateTime :recent, null: false
    DateTime :orig, null: false
    Integer :visit_count, default: 0
  end

  conn.create_table?(:prefs) do
    primary_key :id
    String :key, null: false
    String :value, null: false
  end
end

def migrate(table)
  stupid = :id
  stupid = :tid if table == :bmtags
  # I'm sure there's an easier way.
  keys = SOURCE[table].columns[1..-1]
  SOURCE[table].order_by(Sequel.asc(stupid)).all.each do |row|
    vals = row.values[1..-1]
    input =  Hash[keys.zip(vals)]
    DEST[table].insert(input)
  end
end

SOURCE = Sequel.sqlite("userdata/duke.db")
DEST   = Sequel.connect(ENV['DUKE_DB'])

setup!(DEST)
[:aliases, :bm, :bmtags, :history, :prefs].each{|table| migrate(table)}
