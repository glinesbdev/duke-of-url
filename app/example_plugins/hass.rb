require 'excon'

module HASS
  ::DukeOfUrl::Preferences.register( {
    "hass:token" => { default: '', description: 'The API token for your HASS instance.', valid: /.*/ },
    "hass:url" =>   { default: '', description: 'The URL for your HASS instance. No trailing slash! EG: https://192.168.1.1:8123', valid: /^http/ }, #fix me
    "hass:ignoreTLS" => { default: false, description: 'Ignore certificate errors.', valid: /(?:true|false)/ },
  })

  module ::DukeOfUrl::Input
    module Handlers
      class HaEntityOn < AbstractHandler

        def self.verb
          %q{ha on }
        end

        def message
          @message
        end

        def persist!
          super

          ha.send(action)

          @message = ha.message
        end

        def redirect?
          false
        end

        private

        def ha
          @ha ||= Client.new(last_word)
        end

        def history?
          true
        end

        def action
          :on
        end

      end

      class HaEntityOff < HaEntityOn
        def self.verb
          %q{ha off }
        end

        def action
          :off
        end
      end

      class HaEntityToggle < HaEntityOn
        def self.verb
          %q{ha toggle }
        end

        def action
          :toggle
        end
      end

    end
  end

  class Client
    def initialize(entity)
      @entity      = entity
      @ignore_cert = ::DukeOfUrl::Preferences.bool('hass:ignoreTLS')
      @token       = ::DukeOfUrl::Preferences.fetch('hass:token')
      @message     = nil
      @url         = ::DukeOfUrl::Preferences.fetch('hass:url')
      Excon.defaults[:ssl_verify_peer] = false if @ignore_cert
    end

    def message
      @message
    end

    def off
      speak("turn_off")
    end

    def on
      speak("turn_on")
    end

    def toggle
      speak("toggle")
    end

    private

    def client
      @client ||= Excon.new(@url, headers: {authorization: %Q{Bearer #{@token}}, "content-type": %q{application/json}}, debug: true)
    end

    def entity
      @entity
    end

    def payload
      {entity_id: entity}.to_json
    end

    def speak(verb)
      response = client.post(path: "#{services}/#{verb}", body: payload)
      @message = response.status unless response.status == 200
    end

    def services
      %Q{/api/services/#{service_type}}
    end

    def service_type
      entity.split('.').first
    end
  end

  module Routes
    def self.registered(app)
      app.get "/holyshit" do
        %Q{holy shit!}
      end
    end
  end

end
