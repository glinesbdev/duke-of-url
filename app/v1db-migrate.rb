require 'bundler'
Bundler.setup
require 'sequel'

def self.setup!(conn)

  conn.create_table?(:aliases) do
    primary_key :id
    String :verb, null: false
    String :template, null: false
    DateTime :recent, null: false
    DateTime :orig, null: false
  end

  conn.create_table?(:bmtags) do
    primary_key :tid
    String :tag, null: false
    Integer :bmid, null: false
  end

  conn.create_table?(:bm) do
    primary_key :id
    String :url, null: false
    String :notes
  end

  conn.create_table?(:history) do
    primary_key :id
    String :text, null: false
    DateTime :recent, null: false
    DateTime :orig, null: false
    Integer :visit_count, default: 0
  end

  conn.create_table?(:prefs) do
    primary_key :id
    String :key, null: false
    String :value, null: false
  end
end

COMBINED = ::Sequel.sqlite("userdata/duke.db")
#COMBINED = ::Sequel.connect("postgres://postgres:pgpassword@pghost:5432/duke")
setup!(COMBINED)
aliasdb  = ::Sequel.sqlite('userdata/aliases.db')
bmdb = ::Sequel.sqlite('userdata/bookmarks.db')
historydb = ::Sequel.sqlite('userdata/history.db')
prefsdb = ::Sequel.sqlite('userdata/preferences.db')

aliasdb[:aliases].all.each{|row| COMBINED[:aliases].insert(row)}
bmdb[:bm].all.each{|row| COMBINED[:bm].insert(row)}
bmdb[:tags].all.each{|row| COMBINED[:bmtags].insert(row)}
historydb[:history].all.each{|row| COMBINED[:history].insert(row)}
prefsdb[:prefs].all.each{|row| COMBINED[:prefs].insert(row)}

