ARG BUNDLE_ONLY=production
ARG UID=4567

FROM ruby:3.3.0-alpine as build

ARG BUNDLE_ONLY
ARG UID
ENV BUNDLE_ONLY=${BUNDLE_ONLY}
ENV GEM_HOME /app/vendor/bundle
ENV BUNDLE_PATH $GEM_HOME
ENV PATH $GEM_HOME/bin:$PATH
WORKDIR /app

RUN apk update && apk --no-cache add --virtual build-deps build-base ruby-dev postgresql-dev sqlite-dev libffi-dev\
  && adduser -D -u ${UID} -h /app -g '' duke
COPY --chown=duke:duke app /app
USER duke
RUN bundle config set without 'development test' && bundle config force_ruby_platform true && bundle install


FROM ruby:3.3.0-alpine

ARG BUNDLE_ONLY
ARG UID
ENV BUNDLE_ONLY=${BUNDLE_ONLY}
ENV GEM_HOME /app/vendor/bundle
ENV PATH $GEM_HOME/bin:$PATH
ENV BUNDLE_PATH $GEM_HOME
WORKDIR /app

RUN adduser -D -u ${UID} -h /app -g '' duke
COPY --from=build /app /app
EXPOSE 4567

USER duke
CMD ["bundle", "exec", "puma", "-e", "production", "-b", "tcp://0:4567"]
